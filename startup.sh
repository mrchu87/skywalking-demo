#!/bin/bash

echo "################################################################"
echo "# This one will use maven, docker for demo. Please make sure them"
echo "# available and running"
echo "################################################################"
sleep 3

mvn package -f springboot2-jpa-h2-crud-example/pom.xml 
docker build -t java-service .
docker-compose up -d 
echo "Please wait for 10 minutes before test"
sleep 3
