# skywalking-demo

First, I would like to give a credit on https://github.com/RameshMF with the source code: https://github.com/RameshMF/spring-boot-tutorial/tree/master/springboot2-jpa-h2-crud-example

His code helped me on java service creation.

This one is for skywalking demo purpose. Aimed to skywalking version 6.6.0, elasticsearch 7.5.0.

To get it work, just edit docker-compose.yml file, adapt environment variable SW_AGENT_NAME to your expected service-name and SW_AGENT_COLLECTOR_BACKEND_SERVICES to skywalking server address. 

Afterwards:
./startup.sh

Then give some query to insert or update on java-service and see what will happens on skywalking,

For ex: 

curl -X POST http://<java-service-addr>:8081/api/v1/employees -H 'Content-Type: application/json' -d '{    "firstName": "John",    "lastName": "Cena",    "emailId": "johncena@gmail.com"}'

curl http://<java-service-addr>:8081/api/v1/employees/

That's all!!
