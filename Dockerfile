FROM openjdk:8-jdk

EXPOSE 8080
RUN mkdir -p /opt/app
WORKDIR /opt/app


COPY springboot2-jpa-h2-crud-example/target/springboot2-jpa-crud-example-0.0.1-SNAPSHOT.jar /opt/app/java-service.jar
COPY agent /opt/app/agent

CMD java -javaagent:/opt/app/agent/skywalking-agent.jar -jar java-service.jar

